var electronInstaller = require('electron-winstaller');
//https://ezgif.com/add-text/
//Alegra
//Venty
resultPromise = electronInstaller.createWindowsInstaller({
    appDirectory: 'dist/electron-mat/VentaFacil-win32-ia32/',
    outputDirectory: 'dist/installers/',
    authors: 'Julio Cesar Caicedo Santos',
    exe: 'VentaFacil.exe',
    loadingGif: 'resources/conf.gif',
    noMsi: true,
    setupExe: 'VentaFacilInstaller.exe',
    setupIcon: "resources/icon.ico"
  });

resultPromise.then(() => console.log("It worked!"), (e) => console.log(`No dice: ${e.message}`));
