const routes = [
  {
    path: '/',
    component: () => import('pages/preparar.vue')
  },
  {
    path:'/entrar',
    component: () => import('pages/login.vue')
  },
  {
    path: '/app',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { 
        path: 'inventario',
        component: () => import('pages/Index.vue'),
        children: [
          { path: '/',component: () => import('components/inventario/principal.vue') },
          {
            path: 'inventario',
            component: () => import('components/inventario/inventario/index'),
            children: [
              {path: '/',component: () => import('components/inventario/inventario/lata') },
              {path: 'botella',component: () => import('components/inventario/inventario/botella') },
              {path: 'plastico',component: () => import('components/inventario/inventario/plastico') },
              {path: 'envace',component: () => import('components/inventario/inventario/envace') },
              {path: 'prestado',component: () => import('components/inventario/inventario/prestado') },
              {path: 'unidades',component: () => import('components/inventario/inventario/unidades') }
            ]
          },
          {path: 'productos',component: () => import('components/inventario/productos.vue') },
          {
            path: 'configuracion',
            component: () => import('components/inventario/configuracion/index.vue'),
            children: [
              {path: '/',component: () => import('components/inventario/configuracion/contenido.vue') },
              {path: 'unidades',component: () => import('components/inventario/configuracion/unidades.vue') }
            ]
          }
        ]
      },
      {
        path: 'clientes',
        component: () => import('pages/clientes.vue'),
        children: [
          {path: '/' , component: () => import('components/clientes/credito.vue')},
          {path: 'clientes', component: () => import('components/clientes/index.vue')}
        ]
      },
      {
        path: 'banco',
        component: () => import('pages/banco.vue')
      },
      {
        path: 'deudas',
        component: () => import('pages/deudas.vue')
      }
    ]
  },
  {
    path: '/vender',
    component: () => import('pages/vender.vue')
  },
  {
    path: '/pedidos',
    component: () => import('pages/pedidos.vue')
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
