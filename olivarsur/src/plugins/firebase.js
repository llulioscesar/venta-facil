import firebase from "firebase/app"
import 'firebase/auth'
import 'firebase/firestore'

const config = {
    apiKey: "AIzaSyBL8YLOzMpqA87m6dDwCUb0pWQXc4onFxs",
    authDomain: "jcesar-695cd.firebaseapp.com",
    databaseURL: "https://jcesar-695cd.firebaseio.com",
    projectId: "jcesar-695cd",
    storageBucket: "jcesar-695cd.appspot.com",
    messagingSenderId: "786224671404"
}

export const  fireApp = firebase.initializeApp(config)

export const AUTH = fireApp.auth()

export const FIRESTORE = fireApp.firestore()

export default ({ Vue }) => {
    FIRESTORE.settings({ timestampsInSnapshots: true })

    Vue.prototype.$auth = AUTH
    Vue.prototype.$firestore = FIRESTORE
}