import VueSelectImage from 'vue-select-image'

export default ({Vue}) => {
    Vue.use(VueSelectImage)
}