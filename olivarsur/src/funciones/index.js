import firebase from "firebase";
import "firebase/firestore";
var db = firebase.firestore();
db.settings({ timestampsInSnapshots: true });

const el = {
    uid: () => {
        var doc = db.collection('usuarios').doc()
        return doc.id
    }
}

export default el