import token from './token'
import axios from 'axios'

export default function http(ruta, datos, callback, callbackError) {
    let settings = {
        responseType: 'json',
        responseEncoding: 'utf8',
        headers: {
            'Content-Type': 'application/json',
            'token': token()
        }
    }

    axios.post('https://jcesar.co/plataforma/api/' + (ruta != null ? ruta : ''), datos, settings)
        .then(result => {
            callback(result.data)
        }).catch(err => {
            let error = err.message
            if(err.response){
                if(err.response.data){
                    err = err.response.data.mensaje
                }
            }
            callbackError(error)
        })



}