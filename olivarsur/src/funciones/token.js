import jwt from 'jwt-simple'
import { LocalStorage } from 'quasar'
import moment from 'moment'

export default function token(){
    let user = LocalStorage.get.item('usuario')

    let payload = {
        uid: user.uid,
        rol: user.rol,
        iat: moment().unix(),
        exp: moment().add(2, 'minutes').unix()
    }

    let token = jwt.encode(payload, '@#MiScAmElLoS8462', 'HS384')

    return token
}