import {Inventario, Clientes, Ventas, Disponible, Pedidos} from 'src/database'
export default function(el){
    el.fecha = el.$moment().startOf('day').unix()

      let archivo = el.$moment().format('MMMM_YYYY')
      let lata = 'lata_' + archivo
      let botella = 'botella_' + archivo
      let plastico = 'plastico_' + archivo

      let dbLata = Inventario(lata)
      let dbBotella = Inventario(botella)
      let dbPlastico = Inventario(plastico)
      let clientes = Clientes
      let ventas = Ventas(archivo)
      let disponible = Disponible
      let pedidos = Pedidos

      disponible.find({}, (e, docs) => {
        if(docs.length == 0) {
          dbLata.findOne({fecha: el.fecha}, (e, doc) => {
            if(doc != null){
              let objs = JSON.parse(JSON.stringify(doc.objs))
              objs.forEach(element => {
                element.actual = element.inicial + element.entrada
                delete element.inicial
                delete element.entrada
                delete element.vendidas
                delete element.total
                
              })
              el.objs = JSON.parse(JSON.stringify(el.objs)).concat(JSON.parse(JSON.stringify(objs)))
            }
          })
    
          dbPlastico.findOne({fecha: el.fecha}, (e, doc) => {
            if(doc != null){
              let objs = JSON.parse(JSON.stringify(doc.objs))
              objs.forEach(element => {
                element.actual = element.inicial + element.entrada
                delete element.inicial
                delete element.entrada
                delete element.vendidas
                delete element.total
              })
              el.objs = JSON.parse(JSON.stringify(el.objs)).concat(JSON.parse(JSON.stringify(objs)))
            }
          })
    
          dbBotella.findOne({fecha: el.fecha}, (e, doc) => {
            if(doc != null){
              let objs = JSON.parse(JSON.stringify(doc.objs))
              objs.forEach(element => {
                element.actual = element.inicial + element.entrada
                delete element.inicial
                delete element.entrada
                delete element.vendidas
                delete element.total
              })
              el.objs = JSON.parse(JSON.stringify(el.objs)).concat(JSON.parse(JSON.stringify(objs)))
            }
          })

          setTimeout(() => {
            Disponible.insert(JSON.parse(JSON.stringify(el.objs)), (e, docs) => {})
          }, 1000)
        } else {
          el.objs = JSON.parse(JSON.stringify(docs))
        }
      })

      clientes.find({}, (e, docs) => {
        if(docs.length > 0){
          el.clientes = JSON.parse(JSON.stringify(docs))
        }
      })
      return {clientes: clientes, ventas: ventas, disponible: disponible, pedidos: pedidos}
}