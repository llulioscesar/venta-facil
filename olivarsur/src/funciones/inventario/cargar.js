import { Inventario } from 'src/database'
import verificarActual from './isActual'
import sumarTotales from './sumaTotales'
import {LocalStorage} from 'quasar'
import moment from 'moment'

export default function (file, el) {

    let primeraVez = LocalStorage.get.item('primeraVez')
    el.isInicial = primeraVez

    let fecha = moment(el.fecha);

    let archivo = fecha.format("MMMM_YYYY")

    let archivoLata = "lata_" + archivo;
    let archivoBotella = "botella_" + archivo;
    let archivoPlastico = "plastico_" + archivo;
    let archivoEnvace = "envace_" + archivo;

    el.objs = []
    el.id = 0
    el.total = 0

    let db

    switch (file) {
        case 'botella':
            db = Inventario(archivoBotella)
            break;
        case 'lata':
            db = Inventario(archivoLata)
            break;
        case 'plastico':
            db = Inventario(archivoPlastico)
            break;
        case 'envace':
            db = Inventario(archivoEnvace)
            break
    }

    db.findOne({ fecha: fecha.unix() }, (e, doc) => {
        if (e) {
            el.$q.notify({
                message: "La base de datos se encuentra corrupta por manipulacion manual",
                position: "top-right"
            });
        } else {
            if (doc) {
                el.objs = JSON.parse(JSON.stringify(doc.objs));
                el.totalParte = doc.total;
                el.id = doc._id;
            } else {
                el.objs = [];
                el.id = 0;
                el.total = 0;
                if (verificarActual(fecha) == false) {
                    el.$q.notify({
                        message:"No hay registro de inventario para producto de tipo " + file,
                        position: "top-right"
                    });
                }
            }
        }
    });

    sumarTotales(el, file)

    return db
    
}