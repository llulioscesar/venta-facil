import {Productos} from 'src/database'

Array.prototype.unique = function() {
    var a = this.concat();
    for (var i = 0; i < a.length; ++i) {
      for (var j = i + 1; j < a.length; ++j) {
        if (a[i]._id === a[j]._id) {
            a[i].caja = a[j].caja
            a[i].contenido = a[j].contenido
            a[i].img = a[j].img
            a[i].nombre = a[j].nombre
            a[i].precio = a[j].precio
            a[i].ref = a[j].ref
            a[i].retornable = a[j].retornable
            a[i].unidades = a[j].unidades
            if(a[i].caja == "Botella"){
                let envace = 0
                if(a[i].retornable == true){
                    if(a[i].unidades < 30){
                        let botellas = a[i].unidades * 400
                        envace = botellas + 3000
                    } else {
                        let botellas = a[i].unidades * 100
                        envace = botellas + 3000
                    }
                }
                a[i].total = a[i].vendidas * (a[i].precio + envace);
            } else {
                a[i].total = a[i].vendidas * a[i].precio
            }
          a.splice(j--, 1);
        }
      }
    }
  
    return a;
  };

export default function (el, seccion) {
    let productos = [];
        Productos.find({}, (e, docs) => {
            if (docs) {
                docs.forEach(item => {
                    if (item.caja == seccion) {
                        productos.push(JSON.parse(JSON.stringify(item)));
                    }
                });
            }
        });

        setTimeout(() => {
            console.log(productos)
            if (productos.length > 0) {
                var arr1 = JSON.parse(JSON.stringify(el.objs));
                var arr2 = JSON.parse(JSON.stringify(productos));

                if (arr2.length > arr1.length) {
                    var arr3 = arr1.concat(arr2).unique();

                    arr3.forEach(item => {
                        var x = Object.keys(item);
                        if (x.length == 8) {
                            item.inicial = 0;
                            item.actual = 0;
                            item.entrada = 0;
                            item.total = 0;
                            item.vendidas = 0;
                        }
                    });

                    el.objs = arr3;
                } else {
                    var arr3 = [];
                    arr1.forEach(item => {
                        arr2.forEach(producto => {
                            if (item._id == producto._id) {
                                producto = JSON.parse(JSON.stringify(producto))
                                producto.inicial = item.inicial
                                producto.actual = item.actual
                                producto.entrada = item.entrada
                                producto.vendidas = item.vendidas
                                if(seccion == 'Botella'){
                                    let envace = 0
                                    if(producto.retornable == true){
                                        if(producto.unidades < 30){
                                            let botellas = producto.unidades * 400
                                            envace = botellas + 3000
                                        } else {
                                            let botellas = producto.unidades * 100
                                            envace = botellas + 3000
                                        }
                                    }
                                    producto.total = producto.vendidas * (producto.precio + envace);
                                }else{
                                    producto.total = producto.vendidas * producto.precio
                                }
                                
                                arr3.push(producto);
                            }
                        });
                    });
                    el.objs = JSON.parse(JSON.stringify(arr3));
                }
                el.actualizar();
            }
        }, 1000);
}