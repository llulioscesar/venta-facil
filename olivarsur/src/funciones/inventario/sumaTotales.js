import moment from 'moment'
import {Inventario} from 'src/database'

export default function (el, file) {
    let fecha = moment(el.fecha)

    let archivo = fecha.format('MMMM_YYYY')

    let archivoLata = "lata_" + archivo;
    let archivoBotella = "botella_" + archivo;
    let archivoPlastico = "plastico_" + archivo;
    let archivoEnvace = "envace_" + archivo;

    var s1 = 0, s2 = 0, s3 = 0;

    switch (file) {
        case 'lata':
            Inventario(archivoBotella).findOne({ fecha: fecha.unix() }, (e, doc) => {
                if (doc) {
                    s1 = doc.total;
                }
            });
            Inventario(archivoPlastico).findOne({ fecha: fecha.unix() }, (e, doc) => {
                if (doc) {
                    s2 = doc.total;
                }
            });
            Inventario(archivoEnvace).findOne({ fecha: fecha.unix() }, (e, doc) => {
                if (doc) {
                    s3 = doc.total;
                }
            });
            setTimeout(() => {
                
                el.total = s1 + s2 + el.totalParte + s3;
                el.tempTotal = el.total - el.totalParte;
            }, 1000);
            break

        case 'botella':
            Inventario(archivoLata).findOne({ fecha: fecha.unix() }, (e, doc) => {
                if (doc) {
                    s1 = doc.total;
                }
            });
            Inventario(archivoPlastico).findOne({ fecha: fecha.unix() }, (e, doc) => {
                if (doc) {
                    s2 = doc.total;
                }
            });
            Inventario(archivoEnvace).findOne({ fecha: fecha.unix() }, (e, doc) => {
                if (doc) {
                    s3 = doc.total;
                }
            });
            setTimeout(() => {
                el.total = s1 + s2 + el.totalParte + s3;
                el.tempTotal = el.total - el.totalParte;
            }, 1000);
            break;
        case 'plastico':
            Inventario(archivoLata).findOne({ fecha: fecha.unix() }, (e, doc) => {
                if (doc) {
                    s1 = doc.total;
                }
            });
            Inventario(archivoBotella).findOne({ fecha: fecha.unix() }, (e, doc) => {
                if (doc) {
                    s2 = doc.total;
                }
            });
            Inventario(archivoEnvace).findOne({ fecha: fecha.unix() }, (e, doc) => {
                if (doc) {
                    s3 = doc.total;
                }
            });
            setTimeout(() => {
                el.total = s1 + s2 + el.totalParte + s3;
                el.tempTotal = el.total - el.totalParte;
            }, 1000);
            break;
        case 'envace':
            Inventario(archivoLata).findOne({ fecha: fecha.unix() }, (e, doc) => {
                if (doc) {
                    s1 = doc.total;
                }
            });
            Inventario(archivoBotella).findOne({ fecha: fecha.unix() }, (e, doc) => {
                if (doc) {
                    s2 = doc.total;
                }
            });
            Inventario(archivoPlastico).findOne({ fecha: fecha.unix() }, (e, doc) => {
                if (doc) {
                    s3 = doc.total;
                }
            });
            setTimeout(() => {
                el.total = s1 + s2 + s3 + el.totalParte;
                el.tempTotal = el.total - el.totalParte;
            }, 1000);
            break;
    }

}