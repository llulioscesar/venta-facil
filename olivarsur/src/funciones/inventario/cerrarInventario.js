import {Inventario, Productos} from 'src/database'

export default function(f1, f2, db1, db2, tipo) {

    let data1
    let data2

    if(db1 == db2) {
        data1 = Inventario(db1)
        data2 = data1
    } else{
        data1 = Inventario(db1)
        data2 = Inventario(db2)
    }

    data1.findOne({ fecha: f1 }, (e, doc) => {
      if (tipo != "envace") {
        if (doc) {
          let objs = JSON.parse(JSON.stringify(doc.objs));
          let tem = [];
          let sum = 0

          objs.forEach(item => {
            item = JSON.parse(JSON.stringify(item))
            item.inicial = item.actual;
            item.entrada = 0;
            item.actual = 0;
            item.vendidas = item.inicial;
            let envace = 0
            if(item.retornable == true){
              if(item.unidades < 30){
                envace = item.unidades * 400
              } else {
                envace = item.unidades * 100
              }
              envace = envace + 3000
            }
            item.total = item.vendidas * (item.precio + envace);
            tem.push(item);
          });

          tem.forEach(item => {
            sum = sum + item.total
          })

          let d = {
            objs: JSON.parse(JSON.stringify(tem)),
            fecha: f2,
            total: sum
          };
          console.log(d)

          data2.insert(d, (e, doc) => {});
        } else {
          let productos = [];
          Productos.find({}, (e, docs) => {
            productos = JSON.parse(JSON.stringify(docs));
          });

          setTimeout(() => {
            if (productos.length > 0) {
              let temp = [];

              productos.forEach(item => {
                if (item.caja == tipo) {
                  item.inicial = 0;
                  item.entrada = 0;
                  item.actual = 0;
                  item.vendidas = 0;
                  item.total = 0;
                  temp.push(item);
                }
              });

              let d = {
                objs: temp,
                fecha: f2,
                total: 0
              };

              data2.insert(d, (e, doc) => {});
            }
          }, 1000);
        }
      } else {
        if (doc) {
          let objs = JSON.parse(JSON.stringify(doc.objs));
          let tem = [];
          let sum = 0
          objs.forEach(item => {
            item.inicial = item.actual;
            item.actual = 0
            item.diferencia = item.inicial - item.actual
            item.total = item.precio * item.diferencia;
            tem.push(item);
          });

          tem.forEach(item => {
            sum = sum + item.total
          })

          let d = {
            objs: tem,
            fecha: f2,
            total: sum
          };

          data2.insert(d, (e, doc) => {});
        } else {
          let d = {
            objs: [
              {
                _id: 1,
                nombre: "Caja x 16",
                valor: 9400,
                inicial: 0,
                actual: 0,
                diferencia: 0,
                total: 0
              },
              {
                _id: 2,
                nombre: "Caja x 30",
                valor: 6000,
                inicial: 0,
                actual: 0,
                diferencia: 0,
                total: 0
              },
              {
                _id:3,
                nombre: "Caja x 38",
                valor: 6800,
                inicial: 0,
                actual: 0,
                diferencia: 0,
                total: 0
              },
              {
                _id:4,
                nombre: "Caja vacia",
                valor: 3000,
                inicial: 0,
                actual: 0,
                diferencia: 0,
                total: 0
              },
              {
                _id:5,
                nombre: "Botella normal",
                valor: 100,
                inicial: 0,
                actual: 0,
                diferencia: 0,
                total: 0
              },
              {
                _id:6,
                nombre: "Botella 750",
                valor: 400,
                inicial: 0,
                actual: 0,
                diferencia: 0,
                total: 0
              }
            ],
            fecha: f2,
            total: 0
          };

          data2.insert(d, (e, doc) => {});
        }
      }
    });
  }