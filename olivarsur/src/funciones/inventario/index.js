import isActual from './isActual'
import cargarDB from './cargar'
import sumarTotales from './sumaTotales'
import nuevoInventario from './nuevo'
import guardarFecha from './guardarFecha'
import actualizarCalculos from './actualizarCalculos'
import diaSiguiente from './diaSiguiente'
import sincronizarProductos from './sincronizarProductos'
 
export {
    isActual,
    cargarDB,
    sumarTotales,
    nuevoInventario,
    guardarFecha,
    actualizarCalculos,
    diaSiguiente,
    sincronizarProductos
}