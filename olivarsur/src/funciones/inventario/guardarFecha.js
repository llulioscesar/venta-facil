import { LocalStorage } from 'quasar'
export default function (el, date) {

    let fecha = el.$moment(date).startOf("day").toDate();
    LocalStorage.set("fecha", fecha);
    el.fecha = fecha;

    let fecha1 = el.$moment().startOf("day");
    let fecha2 = el.$moment(el.fecha);

    if (fecha1.unix() == fecha2.unix()) {
        el.isActual = true;
        el.editar = true;
    } else {
        el.isActual = false;
        el.editar = false;
    }
    el.cargar();
}