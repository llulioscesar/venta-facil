import sumaTotales from './sumaTotales'
import { Inventario, Productos, Disponible } from 'src/database'
import {LocalStorage} from 'quasar'


export default function (el, seccion, file, db) {

    if(el.isActual){
        Disponible.remove({}, { multi: true })
    }

    if (el.objs.length == 0) {
        let fechaA = el.$moment(el.fecha);
        let fecha = el.$moment().startOf("day");

        let dif = fecha.diff(fechaA, 'days')

        // comprobar si la fecha elegida es un dia anterior a la fecha actual
        if (dif >= 0 && dif <= 1) {
            LocalStorage.set('editar', true)
            if (file != 'envace') {
                var productos = [];

                Productos.find({}, (e, docs) => {
                    productos = docs;
                });

                setTimeout(() => {
                    if (productos.length == 0) {
                        el.$q
                            .dialog({
                                title: "No hay productos",
                                message:
                                    "No se puede crear el inventario por que no hay productos",
                                ok: "Crear productos",
                                preventClose: true
                            })
                            .then(() => {
                                el.$router.push("/app/inventario/productos");
                            });
                    } else {
                        let l = [];

                        productos.forEach(item => {
                            item = JSON.parse(JSON.stringify(item))
                            if (item.caja == seccion) {
                                item.inicial = 0;
                                item.entrada = 0;
                                item.actual = 0;
                                item.vendidas = 0;
                                item.total = 0;
                                l.push(item);
                            }
                        });

                        if (l.length > 0) {
                            let doc = {
                                objs: JSON.parse(JSON.stringify(l)),
                                fecha: fechaA.unix(),
                                total: 0
                            };

                            db.insert(doc, (e, doc) => {
                                el.id = doc._id;
                                el.objs = JSON.parse(JSON.stringify(doc.objs));
                                el.totalParte = doc.total;
                                el.editar = true;
                                el.isInicial = true;
                                el.cerrado = false;
                                sumaTotales(el, file);
                            });
                        } else {
                            el.$q.notify({
                                message: "No hay productos de tipo " + file,
                                position: "top-right"
                            });
                        }
                    }
                }, 1000);
            } else {

                db.findOne({ fecha: fechaA.unix() }, (e, doc) => {
                    if (!doc) {
                        let d = {
                            objs: [
                                {
                                    _id: 1,
                                    nombre: "Caja x 16",
                                    precio: 9400,
                                    inicial: 0,
                                    diferencia: 0,
                                    actual: 0,
                                    total: 0
                                },
                                {
                                    _id: 2,
                                    nombre: "Caja x 30",
                                    precio: 6000,
                                    inicial: 0,
                                    actual: 0,
                                    diferencia: 0,
                                    total: 0
                                },
                                {
                                    _id: 3,
                                    nombre: "Caja x 38",
                                    precio: 6800,
                                    inicial: 0,
                                    actual: 0,
                                    diferencia: 0,
                                    total: 0
                                },
                                {
                                    _id: 4,
                                    nombre: "Caja vacia",
                                    precio: 3000,
                                    inicial: 0,
                                    actual: 0,
                                    diferencia: 0,
                                    total: 0
                                },
                                {
                                    _id: 5,
                                    nombre: "Botella normal",
                                    precio: 100,
                                    inicial: 0,
                                    actual: 0,
                                    diferencia: 0,
                                    total: 0
                                },
                                {
                                    _id: 6,
                                    nombre: "Botella 750",
                                    precio: 400,
                                    inicial: 0,
                                    actual: 0,
                                    diferencia: 0,
                                    total: 0
                                }
                            ],
                            fecha: fechaA.unix(),
                            total: 0
                        };
                        db.insert(d, (e, doc) => {
                            el.id = doc._id;
                            el.objs = JSON.parse(JSON.stringify(doc.objs));
                            el.totalParte = doc.total;
                            el.editar = true;
                            el.cerrado = false;
                            el.isInicial = true;
                            sumaTotales(el, file);
                        });
                    }
                });

            }


        }
    } else {
        el.$q.notify({
            message: "No se puede sobreescribir el inventario",
            position: "top-right"
        });
    }
}