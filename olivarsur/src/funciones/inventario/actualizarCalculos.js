import {LocalStorage} from 'quasar'
import {Disponible} from 'src/database'
export default function (el, db) {
    if (el.isActual) {
        el.cerrado = false;
        LocalStorage.set("ultimo", el.$moment().startOf("day").toDate());
        Disponible.remove({}, { multi: true })
    }

    let suma = 0;
    let inventario = 0
    el.objs.forEach(item => {
        suma = suma + item.total;
    });

    el.totalParte = suma;
    el.total = el.tempTotal + suma;

    let doc = {
        objs: JSON.parse(JSON.stringify(el.objs)),
        total: suma
    };

    db.update({ _id: el.id }, { $set: doc }, { multi: false }, (e, doc) => {
        if (e) {
            el.$q.notify({
                message: "Ocurrio un error al actualizar el inventario",
                position: "top-right"
            });
        }
    });
}