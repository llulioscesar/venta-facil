import cerrarInventario from './cerrarInventario'
import {LocalStorage} from 'quasar'
import {Disponible} from 'src/database'
export default function (el) {
    if(el.isActual){
        LocalStorage.set("ultimo", el.$moment().startOf("day").toDate());
        Disponible.remove({}, { multi: true })
    }

    let fecha1 = el.$moment(el.fecha);
    let fecha2 = el.$moment(el.fecha).add(1, "days");
    let archivo1 = fecha1.format("MMMM_YYYY");

    let dbLata = "lata_" + archivo1;
    let dbBotella = "botella_" + archivo1;
    let dbPlastico = "plastico_" + archivo1;
    let dbEnvace = "envace_" + archivo1;

    cerrarInventario(fecha1.unix(),fecha2.unix(),dbLata,"lata_" + fecha2.format("MMMM_YYYY"),"Lata");
    cerrarInventario(fecha1.unix(),fecha2.unix(),dbBotella,"botella_" + fecha2.format("MMMM_YYYY"),"Botella");
    cerrarInventario(fecha1.unix(),fecha2.unix(),dbPlastico,"plastico_" + fecha2.format("MMMM_YYYY"),"Botella plastica");
    cerrarInventario(fecha1.unix(),fecha2.unix(),dbEnvace,"envace_" + fecha2.format("MMMM_YYYY"),"envace");
    el.cerrado = true;
    if (el.isActual) {
        el.editar = true;
    } else {
        el.editar = false;
    }
}