const contenido = [
    { label: "175 ml", value: "175 ml", contenido: 175, unidad: "Mililitro", _id: 1 },
    { label: "200 ml", value: "200 ml", contenido: 200, unidad: "Mililitro", _id: 2 },
    { label: "207 ml", value: "207 ml", contenido: 207, unidad: "Mililitro", _id: 3 },
    { label: "225 ml", value: "225 ml", contenido: 225, unidad: "Mililitro", _id: 4 },
    { label: "250 ml", value: "250 ml", contenido: 250, unidad: "Mililitro", _id: 5 },
    { label: "275 ml", value: "275 ml", contenido: 275, unidad: "Mililitro", _id: 6 },
    { label: "269 ml", value: "269 ml", contenido: 269, unidad: "Mililitro", _id: 7},
    { label: "315 ml", value: "315 ml", contenido: 315, unidad: "Mililitro", _id: 8 },
    { label: "300 ml", value: "300 ml", contenido: 300, unidad: "Mililitro", _id: 9 },
    { label: "330 ml", value: "330 ml", contenido: 330, unidad: "Mililitro", _id: 10 },
    { label: "350 ml", value: "350 ml", contenido: 350, unidad: "Mililitro", _id: 11 },
    { label: "355 ml", value: "355 ml", contenido: 355, unidad: "Mililitro", _id: 12 },
    { label: "473 ml", value: "473 ml", contenido: 473, unidad: "Mililitro", _id: 13},
    { label: "710 ml", value: "710 ml", contenido: 710, unidad: "Mililitro", _id: 14 },
    { label: "750 ml", value: "750 ml", contenido: 750, unidad: "Mililitro", _id: 15 },
    { label: "1 L", value: "1 L", contenido: 1, unidad: "Litro", _id: 16 },
    { label: "1.5 L", value: "1.5 L", contenido: 1.5, unidad: "Litro", _id: 17 },
]

export default contenido