const unidades = [
    { label: "x 6", value: 6, _id: 1 },
    { label: "x 12", value: 12, _id: 2 },
    { label: "x 15", value: 15, _id: 3 },
    { label: "x 16", value: 16, _id: 4 },
    { label: "x 18", value: 18, _id: 5 },
    { label: "x 30", value: 30, _id: 6 },
    { label: "x 38", value: 38, _id: 8 },
    { label: "x 24", value: 24, _id: 7 }
]

export default unidades