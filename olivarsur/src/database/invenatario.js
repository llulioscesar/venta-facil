import path from 'path'
import moment from 'moment'
import {remote, ipcRenderer} from 'electron'

const Datastore = remote.getGlobal('neDB')

moment.locale('es')

export default function(mes) {

    var db = null;
    var r = null

    if (process.env.DEV) {
        r = __dirname
    } else {
        r = global.__dirname
        r = r.substring(0,r.indexOf('electron.asar') - 1)
    }

    db = new Datastore({
        filename: path.join(r, "db/invenatrio/" + mes + ".db"),
        autoload: true
    })

    return db
}