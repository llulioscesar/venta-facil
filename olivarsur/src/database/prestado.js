import path from 'path'
import {remote, ipcRenderer} from 'electron'

const Datastore = remote.getGlobal('neDB')

var r = null

if (process.env.DEV) {
    r = __dirname
} else {
    r = global.__dirname
    r = r.substring(0,r.indexOf('electron.asar') - 1)
}

const db = new Datastore({
    filename: path.join(r, 'db/prestado.db'),
    autoload: true
})


export default db