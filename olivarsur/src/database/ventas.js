import path from 'path'
import {remote, ipcRenderer} from 'electron'

const Datastore = remote.getGlobal('neDB')

export default function(mes) {

    var db = null;
    var r = null

    if (process.env.DEV) {
        r = __dirname
    } else {
        r = global.__dirname
        r = r.substring(0,r.indexOf('electron.asar') - 1)
    }

    db = new Datastore({
        filename: path.join(r, "db/ventas/" + mes + ".db"),
        autoload: true
    })

    return db
}