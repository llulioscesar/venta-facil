import Productos from './productos'
import Inventario from './invenatario'
import Contenido from './contenido'
import Unidades from './unidades'
import Clientes from './clientes'
import Ventas from './ventas'
import Disponible from './disponible'
import Pedidos from './pedidos'
import Banco from './banco'
import Deudas from './deudas'
import Prestado from './prestado'
import UnidadesP from './unidadesP'

export {
    Productos,
    Inventario,
    Unidades,
    Contenido,
    Clientes,
    Ventas,
    Disponible,
    Pedidos,
    Banco,
    Deudas,
    Prestado,
    UnidadesP
}