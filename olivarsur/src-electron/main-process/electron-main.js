import { app, BrowserWindow, ipcMain, remote } from 'electron'
import nedb from 'nedb'

global.neDB = nedb



/*ipcMain.on('neDB-load', (e, a) => {
  return new nedb({
    filename: a,
    autoload: true
  });
})*/

if (process.env.PROD) {
  global.__statics = require('path').join(__dirname, 'statics').replace(/\\/g, '\\\\')
}

if(require('electron-squirrel-startup')) app.quit();

let mainWindow

function createWindow () {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    minWidth: 1280,
    minHeight: 720,
    show: false
    //useContentSize: true
  })

  mainWindow.loadURL(process.env.APP_URL)

  mainWindow.on('closed', () => {
    mainWindow = null
  })
  mainWindow.maximize()
  mainWindow.show();
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})





