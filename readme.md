## Git global setup
1. git config --global user.name "Julio César"
2. git config --global user.email "llulioscesar@gmail.com"

## Create a new repository
1. git clone https://gitlab.com/llulioscesar/venta-facil.git
2. cd venta-facil
3. touch README.md
4. git add README.md
5. git commit -m "add README"
6. git push -u origin master

## Existing folder
1. cd existing_folder
2. git init
3. git remote add origin https://gitlab.com/llulioscesar/venta-facil.git
4. git add .
5. git commit -m "Initial commit"
6. git push -u origin master

## Existing Git repository
1. cd existing_repo
2. git remote rename origin old-origin
3. git remote add origin https://gitlab.com/llulioscesar/venta-facil.git
4. git push -u origin --all
5. git push -u origin --tags